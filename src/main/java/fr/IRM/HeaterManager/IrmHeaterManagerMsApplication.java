package fr.IRM.HeaterManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IrmHeaterManagerMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmHeaterManagerMsApplication.class, args);
	}

}
