package fr.IRM.HeaterManager.resource;

import java.io.IOException;

import obix.Obj;
import obix.io.ObixDecoder;

import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclispe.om2m.commons.client.Client;
import org.eclispe.om2m.commons.client.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.IRM.HeaterManager.utils.PostUtils;

@RestController
public class HeaterManagerResource {
	
	private Mapper mapper = new Mapper();
	private static String baseUrl = "http://localhost:8080/~/";
	private static String originator = "admin:admin";
	private static String deviceName = "heater";
	
	//Function to retrieve the last state of the heater in a specific room
	@GetMapping(value="/getHeaterState/{room}")
	public String getHeaterState(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;		
		try {
			resp = client.retrieve(baseUrl + room + "/" + room + "/" + deviceName + "/data/la",originator);
			res = resp.getRepresentation();
			String s = resp.getRepresentation();
			ContentInstance cin = (ContentInstance)mapper.unmarshal(s);
			Obj o = ObixDecoder.fromString(cin.getContent());
			res = Integer.toString((int)o.get("Value").getInt());
		} catch(IOException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	//This service allow the customer to set the heater of the room (by posting a 1 boolean in the cin)
	@PostMapping(value="/setHeater/{room}")
	public String setHeater(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN(room, this.deviceName, 1), baseUrl + room + "/" + room + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return res;
	}
	
	//This service allow the customer to reset the heater of the room (by posting a 0 boolean in the cin)
	@PostMapping(value="/resetHeater/{room}")
	public String resetHeater(@PathVariable String room){
		String res = null;
		Client client = new Client();
		Response resp;
		try {
			resp = PostUtils.createCIN(PostUtils.generateStateCIN(room, deviceName, 0), baseUrl + room + "/" + room + "/" + deviceName + "/data", originator, client);
			res = resp.getRepresentation();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return res;
	}
}
